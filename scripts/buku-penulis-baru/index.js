// @ts-check
  /**
   * You can also import another NPM package
   * const _ = require("lodash");
   * const moment = require("moment");
   */
  
  /**
    *
    * @description handler for buku-penulis-baru
    * @param {import('@mocobaas/server-sdk').ctx} ctx
    * @returns {Promise<import('@mocobaas/server-sdk').returnCtx>}
    * 
    */
  async function handler(ctx) {

    // Transaction Object
    const trxObj = await ctx.moco.tables.createTransaction()
    // Tambah buku
    const dataBuku = await ctx.moco.tables.create({
      table:"buku",
      data:{
        judul: ctx.data.judulBuku,
        tahun_terbit : ctx.data.tahunBuku,
      },
      transaction: trxObj
    });
    // Tambah Penulis
    const dataPenulis = await ctx.moco.tables.create({
      table:"penulis",
      data:{
        nama:ctx.data.namaPenulis,
        alamat:ctx.data.alamatPenulis,
        telp: ctx.data.telpPenulis
      },
      transaction: trxObj
    });
    // Tambah join buku penulis
    await ctx.moco.tables.create({
      table:"join_buku_penulis",
      data:{
        buku_id: dataBuku.id,
        penulis_id: dataPenulis.id
      },
      transaction: trxObj
    });
    // commit transaction
    await trxObj.commit();
    

    return {
      data: "buku-penulis-baru",
      error: null
    }
  }
  
  module.exports = handler;
  