// @ts-check
  /**
   * You can also import another NPM package
   * const _ = require("lodash");
   * const moment = require("moment");
   */
  
  /**
    *
    * @description handler for buku-terbaru
    * @param {import('@mocobaas/server-sdk').ctx} ctx
    * @returns {Promise<import('@mocobaas/server-sdk').returnCtx>}
    * 
    */
  async function handler(ctx) {
    const bukuTerbaru = await ctx.moco.tables.findAll({
      table:"buku",
      orderBy:[{
        order:"desc",
        column:"created_at",
      }],
      limit: 1,
      include:["penerbit"]
    });
    return {
      data: bukuTerbaru.count > 0 ?bukuTerbaru.results[0]:{},
      error: null,
      serializer: "jsonApi"
    }
  }
  
  module.exports = handler;
  