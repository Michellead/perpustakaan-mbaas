// @ts-check
  /**
   * You can also import another NPM package
   * const _ = require("lodash");
   * const moment = require("moment");
   */
  
  /**
     *
     * @description handler for info-buku-terbaru
     * @param {import('@mocobaas/server-sdk').EventContext} ctx
     *
     */
  async function handler(ctx) {
    const {email} = ctx.data
    const bukuTerbaru = await ctx.moco.tables.findAll({
      table:"buku",
      orderBy:[{
        order:"desc",
        column:"created_at",
      }],
      limit: 1,
    })
    console.log(bukuTerbaru);
    await ctx.moco.email.sendWithTemplate("info", email, {
      judul: bukuTerbaru.results[0].judul,
      tahun: bukuTerbaru.results[0].tahun_terbit
    }
    )
    
    console.log("info-buku-terbaru")
  }
  
  module.exports = handler;
  