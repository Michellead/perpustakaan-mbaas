// @ts-check
  /**
   * You can also import another NPM package
   * const _ = require("lodash");
   * const moment = require("moment");
   */
  
  /**
    *
    * @description handler for lihat-cache
    * @param {import('@mocobaas/server-sdk').ctx} ctx
    * @returns {Promise<import('@mocobaas/server-sdk').returnCtx>}
    * 
    */
  async function handler(ctx) {
    const cachedValue = await ctx.moco.cache.get("data-buku-baru");
    return {
      data: cachedValue,
      error: null
    }
  }
  
  module.exports = handler;
  