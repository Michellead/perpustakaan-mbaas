// @ts-check
  /**
   * You can also import another NPM package
   * const _ = require("lodash");
   * const moment = require("moment");
   */
  
  /**
     *
     * @description handler for notif-pengembalian
     * @param {import('@mocobaas/server-sdk').EventContext} ctx
     *
     */
  async function handler(ctx) {
    await ctx.moco.queue.publish("global.pengembalian", { message: "Waktu Pengembalian" });

    console.log("notif-pengembalian")
  }
  
  module.exports = handler;
  