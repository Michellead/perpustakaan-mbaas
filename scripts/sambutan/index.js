// @ts-check
  /**
   * You can also import another NPM package
   * const _ = require("lodash");
   * const moment = require("moment");
   */
  
  /**
     *
     * @description handler for sambutan
     * @param {import('@mocobaas/server-sdk').EventContext} ctx
     *
     */
  async function handler(ctx) {
    const { email,name } = ctx.data
    await ctx.moco.email.sendWithTemplate("sambutan", email, {
      name: name,
      
    })
     await ctx.moco.queue.add(
      "custom.bukuTerbaru",
      { email: email },
      {
        delay: 30000,
        backoff: {
          type: "exponential",
          delay: 5000,
          attempts: 5,
        },
      }
    );
   
  }
  
  module.exports = handler;
  