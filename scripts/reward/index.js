// @ts-check
  /**
   * You can also import another NPM package
   * const _ = require("lodash");
   * const moment = require("moment");
   */
  
  /**
     *
     * @description handler for reward
     * @param {import('@mocobaas/server-sdk').EventContext} ctx
     *
     */
  async function handler(ctx) {
    if (ctx.data.jumlah >= 5) {
      await ctx.moco.tables.create({
        table:"poin",
        data:{
          jumlah: 1,
          user_id: ctx.data.user_id
        }
      })
    }
    
    console.log("reward")
  }
  
  module.exports = handler;
  