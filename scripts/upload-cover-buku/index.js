// @ts-check
  /**
   * You can also import another NPM package
   * const _ = require("lodash");
   * const moment = require("moment");
   */
  
  /**
    *
    * @description handler for upload-cover-buku
    * @param {import('@mocobaas/server-sdk').ctx} ctx
    * @returns {Promise<import('@mocobaas/server-sdk').returnCtx>}
    * 
    */
  async function handler(ctx) {


    const { localFilePath, filePath } = ctx.data;
    const file = await ctx.moco.file.uploadFromLocal(localFilePath, filePath, "cover-buku");
    if (file.ok) {
      return{
        data: file
      };
    } else{
      return{
        data: null,
        error: file.error
      }
    }
  }
  
  module.exports = handler;
  