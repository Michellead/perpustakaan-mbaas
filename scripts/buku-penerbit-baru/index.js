// @ts-check
  /**
   * You can also import another NPM package
   * const _ = require("lodash");
   * const moment = require("moment");
   */
  
  /**
    *
    * @description handler for buku-penerbit-baru
    * @param {import('@mocobaas/server-sdk').ctx} ctx
    * @returns {Promise<import('@mocobaas/server-sdk').returnCtx>}
    * 
    */
  async function handler(ctx) {
    // namaPenerbit, judulBuku, tahunBuku -------> ctx.data
    // Transaction object
    const trxObj = await ctx.moco.tables.createTransaction();
    // Tambah penerbit
    const dataPenerbit = await ctx.moco.tables.create({
      table:"penerbit",
      data: {
        nama: ctx.data.namaPenerbit
      },
      transaction: trxObj
    });
    // Tambah data buku
    await ctx.moco.tables.create({
      table:"buku",
      data: {
        judul: ctx.data.judulBuku,
        tahun_terbit:ctx.data.tahunBuku,
        penerbit_id: dataPenerbit.id
      },
      transaction: trxObj
    });
    // Commit Transaction
    trxObj.commit();

    return {
      data: "buku-penerbit-baru",
      error: null
    }
  }
  
  module.exports = handler;
  